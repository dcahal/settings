# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

# Disable just in time debugging. With drkonqi master we can dump into systemd-coredump
# instead of attaching drkonqi to the running process.
# Only applied when systemd-coredump is used, which currently isn't generally the case but
# we may opt to go down that route eventually.

# The user may override this behavior by explicitly setting
# KCRASH_DUMP_ONLY=0 in ~/.config/environment.d/*.conf
# (or a system-wide location, naturally).

if [ -z "$KCRASH_DUMP_ONLY" ]; then # don't override a user's choice
  if grep --quiet systemd-coredump /proc/sys/kernel/core_pattern; then
    export KCRASH_DUMP_ONLY=1
  fi
fi
