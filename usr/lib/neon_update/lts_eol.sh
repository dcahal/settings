#!/bin/sh
# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

set -e

. /etc/os-release

if [ "$VARIANT_ID" != "user-lts" ]; then
  exit 0
fi

exec kdialog \
  --dontagain neon-eolrc:LTS \
  --icon dialog-warning \
  --sorry 'Neon LTS is at its end of life and no longer gets any updates!\n\nhttps://userbase.kde.org/Neon/LTS/EOL'
