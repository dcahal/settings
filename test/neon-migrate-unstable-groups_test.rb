# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'
require_relative '../usr/lib/neon_update/calamares_groups'

class NeonMigrateUnstableGroups < Minitest::Test
  def setup
    @datadir = File.realpath("#{__dir__}/data/neon-migrate-unstable-groups/")
    @tmpdir = Dir.mktmpdir
    Dir.chdir(@tmpdir)
    FileUtils.cp_r("#{@datadir}/.", '.')
    ENV['ROOT'] = @tmpdir
  end

  def teardown
    FileUtils.rm_r(@tmpdir)
    ENV.delete('ROOT')
  end

  def test_run
    passwd = mock('passwd')
    passwd.expects(:name).returns('xme')
    Etc.expects(:getpwuid).returns(passwd)
    groups = Migrator::ADD_GROUPS.collect { |n| [Etc::Group.new(n, nil, nil, 'xme')] }
    groups += Migrator::REMOVE_GROUPS.collect { |n| [Etc::Group.new(n, nil, nil, 'xme')] }
    Etc.expects(:group).multiple_yields(*groups)
    Migrator::ADD_GROUPS.each do |g|
      Object.any_instance.expects(:system).with('adduser', 'xme', g).returns(true)
    end
    Migrator::REMOVE_GROUPS.each do |g|
      Object.any_instance.expects(:system).with('deluser', 'xme', g).returns(true)
    end

    Migrator.new.run

    assert File.exist?("#{@tmpdir}/var/lib/neon/update-calamares-groups")
  end
end
