# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>

def __test_method_name__
  index = 0
  caller = ''
  until caller.start_with?('test_')
    caller = caller_locations(index, 1)[0].label
    index += 1
  end
  caller
end

require 'minitest/pride'
require 'minitest/autorun'
require 'mocha/minitest'
