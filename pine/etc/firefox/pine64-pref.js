// pinebook specific configuration options

// Disable ctrl+mousewheel
pref("mousewheel.with_control.action", 0);

//disable pinch to zoom function
pref("zoom.maxPercent", 100);

//disable pinch to zoom function
pref("zoom.minPercent", 100);
